#!/bin/bash 

. ./scripts.cfg

IFS=$'\r\n' GLOBIGNORE='*' command eval  'Lines=($(cat ./titles.txt))'

CurrentLine=0;
echo " "
echo -e " \e[33m${Lines[$CurrentLine]}\e[39m"
echo " ${Lines[$CurrentLine + 1]}"
echo "${Lines[$CurrentLine]}" > $path/currenttitle.txt

while true; do
	read -sN1 key # 1 char (not delimiter), silent
	# catch multi-char special key sequences
	read -sN1 -t 0.0001 k1
	read -sN1 -t 0.0001 k2
	read -sN1 -t 0.0001 k3
	key+=${k1}${k2}${k3}

	case "$key" in
	  k|l|$'\e[B'|$'\e0B'|$'\e[C'|$'\e0C')  # cursor down, right: next item
		{
			let "LinesLimit=${#Lines[@]}-1"
			if [ $CurrentLine -lt $LinesLimit ]
			then
				let "CurrentLine+=1"
				echo -en "\e[3A"
				echo -e "\r\e[0K ${Lines[$CurrentLine - 1]}"
				echo -e "\r\e[0K \e[33m${Lines[$CurrentLine]}\e[39m"
				echo -e "\r\e[0K ${Lines[$CurrentLine + 1]}"
    				echo "${Lines[$CurrentLine]}" > $path/currenttitle.txt
			else
				echo -en "\e[1A"
				echo -e "\r\e[0K \e[34mLast Line\e[39m"
			fi
		};;
	  i|j|$'\e[A'|$'\e0A'|$'\e[D'|$'\e0D')  # cursor up, left: previous item
		{
			if [ $CurrentLine -gt 0 ]
			then
				let "CurrentLine-=1"
				echo -en "\e[3A"
				echo -e "\r\e[0K ${Lines[$CurrentLine - 1]}"
				echo -e "\r\e[0K \e[33m${Lines[$CurrentLine]}\e[39m"
				echo -e "\r\e[0K ${Lines[$CurrentLine + 1]}"
    				echo "${Lines[$CurrentLine]}" > $path/currenttitle.txt
			else
				echo -en "\e[3A"
				echo -e "\r\e[0K \e[34mFirst Line\e[39m"
				echo -e "\r\e[0K \e[33m${Lines[$CurrentLine]}\e[39m"
				echo -e "\r\e[0K ${Lines[$CurrentLine + 1]}"
			fi
		};;
	esac

done

