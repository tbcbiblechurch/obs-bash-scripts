# obs-bash-scripts

## Bash scripts to aide in controlling OBS  

Change the value of $path in scripts.cfg before using.  

Each script will generate a text file in the $path. The text files can be
loaded in OBS by creating a text source and selecting "Read from file".
